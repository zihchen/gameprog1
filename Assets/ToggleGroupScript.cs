﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ToggleGroupScript : MonoBehaviour
{
    ToggleGroup toggleGroupInstance;


    public Toggle currentSelection
    {
        get { return toggleGroupInstance.ActiveToggles().FirstOrDefault(); }
    }
    // Start is called before the first frame update
    void Start()
    {
        toggleGroupInstance = GetComponent<ToggleGroup>();
        Debug.Log(currentSelection.name);
    }

 
}
