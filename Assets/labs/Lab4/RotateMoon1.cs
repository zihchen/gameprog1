﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateMoon1 : MonoBehaviour
{
    public float moon1Speed=30;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 parentPosition = transform.parent.transform.position;

        transform.RotateAround(parentPosition, Vector3.forward, moon1Speed * Time.deltaTime);
    }
}
