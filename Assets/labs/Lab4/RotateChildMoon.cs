﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateChildMoon : MonoBehaviour
{
    public float childMoonSpeed = 30;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 childMoonPos = transform.GetChild(0).transform.position; //get planet's first child
        transform.GetChild(0).RotateAround(transform.position, Vector3.forward, childMoonSpeed * Time.deltaTime);
    }
}
