﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireScript : MonoBehaviour
{
    public float speed;
    public bool isLeftFacing;
    // Start is called before the first frame update
    void Start()
    {
        if(speed ==0)
        {
            Debug.LogError("You forgot to set the speed.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isLeftFacing)
        {
            transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0));
        }
        else
        {
            SpriteRenderer sprite = GetComponent<SpriteRenderer>();
            if (sprite.flipX == false)
                sprite.flipX = true;

            transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        }
    }
}
