﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cannonScipt : MonoBehaviour
{
    public GameObject fireball;

    // Start is called before the first frame update
    void Start()
    {
        if (fireball == null)
            Debug.LogError("you forgot to set the prefab");
    }

    // Update is called once per frame
    void Update()
    {
        Animator anim = GetComponent<Animator>();

        if (Input.GetButtonDown("Jump"))
        {
            anim.SetBool("Fireball", true);
        }
    }

    public void SpawnFireball(Object nullobj)
    {
        GameObject obj = GameObject.Instantiate<GameObject>(fireball);
        Destroy(obj, 2);
    }

    public void SwitchTransition(Object nullobj)
    {
        Animator anim = GetComponent<Animator>();
        anim.SetBool("Fireball", false);
    }
}
