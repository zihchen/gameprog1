﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon2Scipt : MonoBehaviour
{
    public GameObject fireball;
    // Start is called before the first frame update
    void Start()
    {
        if (fireball == null)
            Debug.LogError("you forgot to set the prefab");
    }

    // Update is called once per frame
    void Update()
    {
        Animator anim = GetComponent<Animator>();

        if (Input.GetButtonDown("c2shot"))
        {
            anim.SetBool("fireball2", true);
            InvokeRepeating("SpawnFireball", 0.9f, 0);
        }
    }

    public void SpawnFireball()
    {
        GameObject obj = GameObject.Instantiate<GameObject>(fireball);
        //Debug.Log("abc");
        Destroy(obj, 2);
    }

    public void SwitchTransition(Object nullobj)
    {
        Animator anim = GetComponent<Animator>();
        anim.SetBool("fireball2", false);
    }
}
