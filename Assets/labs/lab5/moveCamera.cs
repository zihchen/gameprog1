﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveCamera : MonoBehaviour
{
    public float cameraHorizontalMove=0;
    public float cameraVerticalMove=0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(cameraHorizontalMove,cameraVerticalMove,transform.position.z);
    }
}
