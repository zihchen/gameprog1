﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverOver : MonoBehaviour
{
    MouseWorldPos mwp;
    public Bounds bounds;
    public Color hoverColor;
    // Start is called before the first frame update
    void Start()
    {
        mwp = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<MouseWorldPos>();
        if (mwp == null)
        {
            Debug.LogError("cannot find correct mwp object");
        }
    }

    // Update is called once per frame
    void Update()
    {
        SpriteRenderer sprite = GetComponent<SpriteRenderer>();
        bounds = sprite.bounds;

        Vector3 wordPoint = new Vector3(mwp.mouseWorldPostion.x,
                                          mwp.mouseWorldPostion.y,
                                          transform.position.z);

        if (sprite.bounds.Contains(wordPoint))
        {
            sprite.color = hoverColor;
        }
        else
        {
            sprite.color = Color.white;
        }
    }
}
