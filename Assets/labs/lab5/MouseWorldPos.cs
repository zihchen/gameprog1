﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseWorldPos : MonoBehaviour
{
    public Vector3 mouseWorldPostion;
    public Vector3 wordPt;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Camera camera = GetComponent<Camera>();

        wordPt = camera.ScreenToWorldPoint(
                        new Vector3(Input.mousePosition.x,
                                    Input.mousePosition.y,
                                    Input.mousePosition.z
                        ));

        mouseWorldPostion = new Vector3(wordPt.x, wordPt.y, wordPt.z);
    }
}
