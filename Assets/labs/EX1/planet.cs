﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class planet : MonoBehaviour
{
    public float moveSpeed;
    public float rotateSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalMove = Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime;

        float verticalMove = Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime;

        transform.Translate(horizontalMove, verticalMove, 0, Space.World);

        transform.Rotate(Vector3.forward * rotateSpeed * Time.deltaTime, Space.Self);

   
    }
}
