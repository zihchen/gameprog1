﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moonSprite : MonoBehaviour
{

    public Vector3 MousePosition;
    public Vector3 LastPosition;
    public float scale;
    public Vector2 Velocity;


    private Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        GameObject obj = GameObject.FindWithTag("MainCamera");

        if(obj != null)
        {
            cam = obj.GetComponent<Camera>();
        }
        else
        {
            Debug.LogError("cannot find main camera.");
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        // store the last mouse position
        LastPosition = MousePosition;

        //grab the current mouse position in world coordinates
        MousePosition = cam.ScreenToWorldPoint(Input.mousePosition);

        //calculate velocity from the vector between these points
        Velocity = (MousePosition - LastPosition) * scale;

        transform.position = new Vector3(MousePosition.x, MousePosition.y, transform.position.z
            );

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        //apply the velocity to the other object
        collision.otherRigidbody.velocity += Velocity;

    }
}
