﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[SerializeField]

public class input : MonoBehaviour
{
    private float rSpeed; //rotation Speed for gameObject
    private float mSpeed; //movement speed for gameobject
    private float sSpeed; //scaling speed for gameobject

    private float moveStep; 
    private float scaleAdd;
    
    // Start is called before the first frame update
    void Start()
    {
        GameObject obj = GameObject.FindGameObjectWithTag("GameManager");
        GameManager sm = obj.GetComponent<GameManager>();

        rSpeed = sm.rotationSpeed; //get the rotationSpeed from sm
        mSpeed = sm.movementSpeed; //get the movementSpeed from sm
        sSpeed = sm.scalingSpeed;  //get the scalingSpeed from sm

        moveStep = 0.001f; //initalize the move step
        scaleAdd = 0.001f; //initalize the value which is added to scale
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward * rSpeed * Time.deltaTime, Space.Self); //rotation

        transform.position = new Vector3(transform.position.x + mSpeed * moveStep, transform.position.y, transform.position.z);//movement

        transform.localScale = new Vector3(transform.localScale.x + sSpeed * scaleAdd,transform.localScale.y + sSpeed * scaleAdd, transform.localScale.z);//scaling

        //set up the moving limit for object:
        if(transform.position.x>8)
        {
            moveStep = -0.001f;
        }
        if (transform.position.x < -8)
        {
            moveStep = 0.001f;
        }

        //set up the scaling limit for object:
        if (transform.localScale.x>3.5)
        {
            scaleAdd = -0.001f;
        }
        if (transform.localScale.x < -3.5)
        {
            scaleAdd = 0.001f;
        }
    }
}
