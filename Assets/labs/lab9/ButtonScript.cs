﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
    public InputField ipt;

    public ToggleGroup tg;

    public void onClick()
    {
        Debug.Log("User input : " + ipt.text);
        string temp1 = ipt.text;
        string temp2 = null;
        var toggles = tg.GetComponentsInChildren<Toggle>();
        for(int i = 0; i < 4; i++)
        {
            if(toggles[i].isOn == true)
            {
                temp2 = toggles[i].GetComponentInChildren<Text>().text;
                Debug.Log("User choose " + temp2 + " from ratio buttoms.");
            }
        }


        int num1 = int.Parse(temp1);
        int num2 = int.Parse(temp2);

        int result = num1 * num2;
        Debug.Log("So, " + num1 + " x " + num2 + " = " + result);

    }
}
