﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wallStatus : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.name + " collided with the " + this.gameObject.name);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.gameObject.name + " triggered the " + this.gameObject.name);
    }
}
