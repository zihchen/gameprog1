﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball2Script : MonoBehaviour
{

    public float speed;
    public bool isLeftFacing;

    private float xPos;
    private float yPos;
    // Start is called before the first frame update
    void Start()
    {
        if (speed == 0)
        {
            Debug.LogError("You forgot to set the speed.");
        }

        xPos = Random.Range(-10, 10);
        yPos = Random.Range(-10, 10);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(xPos* Time.deltaTime, yPos * Time.deltaTime, 0));
    }
}

