﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInputEx2 : MonoBehaviour
{
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Those logic is come from TA's planet video, and it can wrok!
        float horizonMove = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        float vertucalMove = Input.GetAxis("Vertical") * speed * Time.deltaTime;

        transform.Translate(horizonMove, vertucalMove, 0, 0);
        /*
        //For the following code,I just imitate professor's logic from the tutoring video
        //but it is fail to move up and move down.

        float axis = Input.GetAxis("Horizontal");
        Debug.Log("Horizontal: " + axis);
        transform.Translate(new Vector3(axis * speed * Time.deltaTime, 0f, 0f));

        float axis2 = Input.GetAxis("Vertical");
        Debug.Log("Vertical: " + axis2);
        transform.Translate(new Vector3(axis2 * speed * Time.deltaTime, 0f, 0f));
        */
    }
}
